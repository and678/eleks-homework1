﻿using System;
namespace HomeWork1
{
	class Program
	{
		public static void Main()
		{
			Author[] authorArray = {
				new Author("Petro"),
				new Author("Andriy"),
				new Author("Mukola")
			};
			Library test1 = new Library("Biblioteka", new [] {
				new Category("NULP Library", new [] {
					new Book("abc", authorArray[0], 5),
					new Book("eee", authorArray[1], 5),
					new Book("fff", authorArray[2], 5)
				}),
				new Category("Stefanuka", new [] {
					new Book("ssss", authorArray[0], 5),
				}),
				new Category("LNU Library", new [] {
					new Book("123", authorArray[1], 5),
					new Book("4444", authorArray[2], 5),
				})
			});

			Console.WriteLine(test1 + "\n" 
				+ test1.Categories[0] + "\n" 
				+ test1.Categories[1].Books[0]);

			Console.Read();
		}
	}
}