﻿using System;

namespace HomeWork1
{
	public class Library : ICountingBooks, IComparable<Library>
	{
		public string Name { get; set; }

		public Category[] Categories { get; set; }

		public Library()
		{
			Name = "";
			Categories = null;
		}

		public Library(string newName, Category[] newCats)
		{
			Name = newName;
			Categories = newCats;
		}

		public int CompareTo(Library other)
		{
			return Categories.Length.CompareTo(other.Categories.Length);
		}

		public int CountBooks()
		{
			throw new NotImplementedException();
		}

		public override string ToString()
		{
			return $"Library: {Name}. Has {Categories.Length} categories.";
		}
	}
}