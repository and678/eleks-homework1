﻿using System;
using System.Collections.Generic;

namespace HomeWork1
{

	public class Author : IAuthor, IComparable<Author>
	{
		public string Name { get; set; }
		public List<Book> Books { get; set; }

		public Author()
		{
			Name = "";
			Books = new List<Book>();
		}

		public Author(string newName)
		{
			Name = newName;
			Books = new List<Book>();
		}

		public int CompareTo(Author other)
		{
			return this.Books.Count.CompareTo(other.Books.Count);
		}

		public int CountBooks()
		{
			return Books.Count;
		}
	}
}