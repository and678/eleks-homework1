﻿using System;

namespace HomeWork1
{
	public class Book : AbstractBook, IComparable<Book> 
	{

		public Book() : base()
		{
		}

		public Book(string newName, Author newAuthor = null, int newPages = 0)
			: base(newName, newAuthor, newPages)
		{
			Author?.Books.Add(this);
		}


		public int CompareTo(Book other)
		{
			return this.Pages.CompareTo(other.Pages);
		}

		public override string ToString()
		{
			return $"Book: {Name}. Was written by: {this.Author.Name}. Has {this.Pages} pages.";
		}

	}
}