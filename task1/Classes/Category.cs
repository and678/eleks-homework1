﻿using System;

namespace HomeWork1
{
	public class Category : ICountingBooks, IComparable<Category>
	{
		public string Name { get; set; }

		public Book[] Books { get; private set; }

		public Category()
		{
			Name = "";
			Books = null;
		}

		public Category(string newName, Book[] newBooks)
		{
			Name = newName;
			Books = newBooks;
		}

		public int CompareTo(Category other)
		{
			return Books.Length.CompareTo(other.Books.Length);
		}

		public int CountBooks()
		{
			return Books.Length;
		}

		public override string ToString()
		{
			return $"Library: {Name}. Has {Books.Length} books.";
		}
	}
}