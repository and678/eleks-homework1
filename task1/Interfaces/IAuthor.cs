﻿using System;
using System.Collections.Generic;

namespace HomeWork1
{
	interface IAuthor : ICountingBooks
	{
		String Name { get; set; }
		List<Book> Books { get; set; }
	}
}