﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
	class Program
	{
		static void Print(int[,] matrix)
		{
			int counter = 0;
			foreach (var n in matrix) {
				counter++;
				Console.Write(n + "\t" + ((counter % 5 == 0) ? "\n" : ""));
			}
		}

		static void Print2(int[][] matrix)
		{
			foreach (var n in matrix) {
				foreach(var j in n) {
					Console.Write(j);
				}
				Console.WriteLine();
			}
		}

		static int EvaluateRow(int[] row)
		{
			var dictionary = new Dictionary<int, int>();
			foreach (int num in row) {
				if (dictionary.ContainsKey(num))
				{
					dictionary[num]++;
				}
				else
				{
					dictionary.Add(num, 1);
				}
			}
			return dictionary.Values.Max();
		}

		static void Main()
		{
			int[,] matrix =
			{
				{2, 5, -6, 7, 2},
				{-2, 1, 5, 4, 9},
				{1, -2, 0, 1, 0},
				{6, 0, 3, 7, 8},
			};
			Console.WriteLine("Task 1");

			Print(matrix);

			int nonNegative = -1;
			for (int i = 0; i < matrix.GetLength(1); i++) {
				bool negative = false;
				for (int j = 0; j < matrix.GetLength(0); j++) { 
					if (matrix[j,i] < 0) {
						negative = true;
						break;
					}
				}
				if (negative == false) {
					nonNegative = i;
					break;
				}	
			}
			Console.WriteLine("First non-negative column: " + nonNegative);

			Console.WriteLine("Task 2");

			int[][] matrix2 = new int[3][] {
				new int[] {1, 1, 0},
				new int[] {2, 2, 2},
				new int[] {4, 2, 1}
			};

			Array.Sort<int[]>(matrix2, (x, y) => EvaluateRow(x).CompareTo(EvaluateRow(y)));

			Print2(matrix2);

			Console.WriteLine(EvaluateRow(matrix2[2]));
			Console.Read();

		}
		
	}
}
