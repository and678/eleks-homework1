﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
	class Program
	{
		static bool HasNegative(int[] row)
		{
			foreach(var i in row) {
				if (i < 0)
				{
					return true;
				}
			}
			return false;
		}

		static bool IsSaddle(int[][] matrix, int x, int y)
		{
			foreach (int num in matrix[x]) {
				if (num < matrix[x][y])
				{
					return false;
				}
			}
			foreach (var row in matrix) {
				if (row[y] > matrix[x][y])
				{
					return false;
				}
			}
			return true;
		}

		static void Main(string[] args)
		{
			int[][] matrix =
			{
				new int[] {2, 5, -6, 7, 2},
				new int[] {12, 11, 5, 24, 99},
				new int[] {1, -2, -7, 1, 0},
				new int[] {6, 0, -8, 7, 8}
			};
			Console.WriteLine("Task 1");
			for (int i = 0; i < matrix.Length; i++) {
				int sum = 0;
				if (HasNegative(matrix[i])) {
					foreach(int j in matrix[i]) {
						sum += j;
					}
					Console.WriteLine($"Row {i} has negative element and its sum is {sum}.");
				}
			}

			Console.WriteLine("Task2");

			for (int i = 0; i < matrix.Length; i++) {
				for (int j = 0; j < matrix[i].Length; j++) {
					if (IsSaddle(matrix, i, j))
					{
						Console.WriteLine($"Element {matrix[i][j]} located at x={i}, y={j} is saddle!");
					}
				}
			}
			Console.Read();
		}
	}
}
