﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
	class Program
	{
		static bool HasNegative(int[] row)
		{
			foreach (var i in row) {
				if (i < 0)
				{
					return true;
				}
			}
			return false;
		}

		static bool CheckRowCol(int[][] matr, int row, int col)
		{
			for (int i = 0; i < 8; i++) {
				if (matr[row][i] != matr[i][col])
				{
					return false;
				}
			}
			return true;
		}

		static void Main(string[] args)
		{
			int[][] matrix =
			{
				new int[8] {0, 0, 0, 1, 0, 0, 0, 0},
				new int[8] {0, 0, 0, 2, 0, 0, 0, 0},
				new int[8] {0, -1, 0, 2, 0, 0, 0, 0},
				new int[8] {1, 2, 2, 2, 2, 2, 2, 2},

				new int[8] {0, 0, 0, 2, 0, 0, 0, 0},
				new int[8] {0, -1, 0, 2, 0, 0, 0, 0},
				new int[8] {0, 0, 0, 2, 0, 0, 0, 0},
				new int[8] {0, 0, 0, 2, 0, 0, 0, 0}
			};
			Console.WriteLine("Task 1");
			for (int i = 0; i < matrix.Length; i++) {
				for (int j = 0; j < matrix[i].Length; j++) {
					if (CheckRowCol(matrix, i, j)) {
						Console.WriteLine($"\t{i} row equals {j} column.");
					}
				}
			}

			Console.WriteLine("Task 2");
			for (int i = 0; i < matrix.Length; i++) {
				int sum = 0;
				if (HasNegative(matrix[i])) {
					foreach (int j in matrix[i]) {
						sum += j;
					}
					Console.WriteLine($"\tRow {i} has negative element and its sum is {sum}.");
				}
			}
			Console.Read();
		}
	}
}
